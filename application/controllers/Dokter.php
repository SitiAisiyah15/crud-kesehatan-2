<?php

Class Dokter extends CI_Controller {

        function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('url'));
        $this->load->model('dokter_model');
        }
 
        public function index(){ 
            $data['tb_jadwal']=$this->dokter_model->getAll();
            $this->load->view('tampil_dokter',$data);
        }
     
        public function ambilData(){
            $data = $this->dokter_model->tampil_tabel();
            echo json_encode($data);
        }
     
        function ambilDataByIdDokter(){
            $id_dokter = $this->input->post('id_dokter');
            $data = $this->dokter_model->getDataByIdDokter($id_dokter);
            echo json_encode($data);
        }
     
        function hapusData(){
            $id_dokter = $this->input->post('id_dokter');
            $data = $this->dokter_model->deleteData($id_dokter);
            echo json_encode($data);
        }
     
        function tambahData(){
            $id_dokter = $this->input->post('id_dokter');
            $nama_dokter = $this->input->post('nama_dokter');
            $alamat = $this->input->post('alamat');
            $notlp = $this->input->post('notlp');
            $email = $this->input->post('email');
            $spesialis = $this->input->post('spesialis');
            $jadwal = $this->input->post('jadwal');
     
            $data = ['id_dokter' => $id_dokter, 
                    'nama_dokter' => $nama_dokter, 
                    'alamat' => $alamat, 
                    'notlp' => $notlp, 
                    'email'=> $email, 
                    'spesialis' => $spesialis, 
                    'jadwal'=> $jadwal];

            $data = $this->dokter_model->insertData($data);
            echo json_encode($data);
        }
     
        function perbaruiData(){
            $id_dokter = $this->input->post('id_dokter');
            $nama_dokter = $this->input->post('nama_dokter');
            $alamat = $this->input->post('alamat');
            $notlp = $this->input->post('notlp');
            $email = $this->input->post('email');
            $spesialis = $this->input->post('spesialis');
            $jadwal = $this->input->post('jadwal');
     
            $data = ['id_dokter' => $id_dokter, 
                    'nama_dokter' => $nama_dokter, 
                    'alamat' => $alamat, 
                    'notlp' => $notlp, 
                    'email'=> $email, 
                    'spesialis' => $spesialis, 
                    'jadwal'=>$jadwal];
     
            $data = $this->dokter_model->updateData($id_dokter,$data);
            echo json_encode($data);
        }
        
}

 